# Bubby's crux ports :D

This ports collection includes various programs/libs/langs that I wanted to use.
Some might find them useful so I thought I'd make it properly usable.

Check out [bubby.git](./bubby.git) for the httpup update file.
